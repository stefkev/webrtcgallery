var express = require("express");
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var mapper = require("array");
var OpenTok = require("opentok")
var shortid = require('shortid');

var sessionHolder = mapper();
var rooms = mapper();

var port = process.env.PORT || 3000;


var apiKey = '45468232';
var apiSercret = 'a74e22e2ae3720cfb8cbb904d28fa730570aa90a';

var opentok = new OpenTok(apiKey, apiSercret)


var createSession = function(callback){
    opentok.createSession(function(err, session){
        sessionHolder.push({id:session.id});
        return callback({id:session.sessionId});
    })
}

var generateToken = function(sessionId, callback){
    var token = opentok.generateToken(sessionId)
    return callback({token:token});
}

app.set("view engine", "jade");
app.use(express.static('public'));

app.get('/',function(req, res){
    res.render('index');
})

app.get('/generateSession',function(req, res){
    createSession(function(sessionData){
        var id = sessionData.id;
        generateToken(id, function(genTokenData1){
            var token1 = genTokenData1.token;
            generateToken(id, function(getTokenData2){
                var token2 = getTokenData2.token;
                var data = {};
                var roomId = shortid.generate();
                rooms.push({roomId:roomId});
                data.roomId = roomId;
                data.sessionId = id;
                data.hostToken = token1;
                data.guestToken = token2;
                sessionHolder.push(data);

                res.json({
                    hostUrl: "conversation?roomId="+data.roomId+"&type=host",
                    guestUrl: "conversation?roomId="+data.roomId+"&type=guest"
                })
            })
        })
    })
})
app.get("/conversation", function(req, res){
    var roomId = req.query.roomId;
    var type =  req.query.type;


    sessionHolder.forEach(function(data){
        if(data.roomId == roomId){
            if(type=='host'){
                res.render('conversation',{roomId: roomId, sessionId: data.sessionId, apiKey: apiKey, token: data.hostToken, type:'host'})
                return;
            } else if(type=='guest'){
                res.render('conversation',{roomId: roomId, sessionId: data.sessionId, apiKey: apiKey, token: data.guestToken, type:'guest'})
                return;
            }
        }
    });
})

io.on('connection',function(socket){
    socket.on('joinMe',function(data){
        var room = data.roomId;
       socket.join(room);
    })
    socket.on('sendCmd',function(cmdData){
        socket.to(cmdData.roomId).emit('cmdData',cmdData);
    })
})




server.listen(port, function(){
    console.log("Server started on port 3000");
})