
var iHaveControl = false;

var url = window.location.host;





function initializeSession(roomId, apiKey, sessionId, token, type) {

    var $fotoramaGalleryOne = $('#fotoramaGalleryOne').fotorama();
    var $fotoramaGalleryTwo = $('#fotoramaGalleryTwo').fotorama();

    var fotorama1 = $fotoramaGalleryOne.data('fotorama');
    var fotorama2 = $fotoramaGalleryTwo.data('fotorama');

    if (type=='host'){
        iHaveControl = true;

    } if (type=='guest'){
        iHaveControl = false;
        $('#fotorama').css('pointer-events','none')
        $("#btnGuest").prop("disabled", true);

    }



    var socket = io.connect(url);

    socket.on('connect',function(){
        socket.emit('joinMe',{
            roomId: roomId,
            type: type
        })
    })
    socket.on('cmdData',function(cmdData){

        if (cmdData.youHaveControl == true){
            if(type == 'host'){
                iHaveControl = true;
                $('#fotoramaGalleryOne').css('pointer-events','auto');
                $('#fotoramaGalleryTwo').css('pointer-events','auto');
                $("#btnHost").attr("disabled", false);
            }
            if(type == 'guest'){
                iHaveControl=true;
                $('#fotoramaGalleryOne').css('pointer-events','auto');
                $('#fotoramaGalleryTwo').css('pointer-events','auto');
                $("#btnGuest").attr("disabled", false);
            }
        }
        if(cmdData.imgIndex && iHaveControl==false){
            if(cmdData.gallery == 1){
                fotorama1.show(cmdData.imgIndex.activeIndex);
            }
            if(cmdData.gallery == 2){
                fotorama2.show(cmdData.imgIndex.activeIndex);
            }
        }
    })

    $("#btnHost").on('click',function(event){

        event.stopPropagation();
        iHaveControl = false;
        $("#btnHost").attr("disabled", true);
        $('#fotoramaGalleryOne').css('pointer-events','none');
        $('#fotoramaGalleryTwo').css('pointer-events','none');
        socket.emit('sendCmd',{
            roomId: roomId,
            youHaveControl:true
        })
    })

    $("#btnGuest").on('click',function(event){
        event.stopPropagation();
        iHaveControl = false;
        $("#btnGuest").attr("disabled", true);
        $('#fotoramaGalleryOne').css('pointer-events','none');
        $('#fotoramaGalleryTwo').css('pointer-events','none');
        socket.emit('sendCmd',{
            roomId: roomId,
            youHaveControl: true
        })
    })

    $('#fotoramaGalleryOne').click(function(){
        if(iHaveControl == true){
            socket.emit('sendCmd',{roomId: roomId, gallery: 1, imgIndex:fotorama1})
        }
    })
    $('#fotoramaGalleryTwo').click(function(){
        if(iHaveControl == true){
            socket.emit('sendCmd',{roomId: roomId, gallery:2, imgIndex:fotorama2})
        }
    })

    var session = OT.initSession(apiKey, sessionId);


    session.on('streamCreated', function(event) {
        session.subscribe(event.stream, 'subscriber', {
            insertMode: 'append',
            width: '100%',
            height: '100%'
        });
    });


    // Connect to the Session
    session.connect(token, function(error) {
        // If the connection is successful, initialize a publisher and publish to the session
        if (!error) {
            var publisher = OT.initPublisher('publisher', {
                insertMode: 'append',
                width: '100%',
                height: '100%'
            });

            session.publish(publisher);

        } else {
            console.log('There was an error connecting to the session:', error.code, error.message);
        }

    });

}